var app = app || {};
var spBreak = 737;

app.init = function() {
  //app.btnTop(); // Back To Top
  app.fixMenu(); // Tab
};

app.isMobile = function() {
  return $(window).width() < spBreak;
};

app.fixMenu = function() {

  $('.l-sideMenu').scrollToFixed({
    limit: function() {
      var limit = $('.l-footer').offset().top - $('.l-sideMenu').outerHeight(true) - 100;
      return limit;
    },
    zIndex: 999,
    marginTop: 10
  });
  
  $('.contactBtn').scrollToFixed({
    marginTop: 10,
    limit: function() {
      var limit = $('.contact').offset().top - $('.contactBtn').outerHeight(true) - 50;
      return limit;
    },
  });

  if($('.slider').length > 0 ) {
    $('.slider').bxSlider({
      controls: false,
      auto: true
    });
  }
  if($('.l-mainHome .inner').length > 0 ) {
    $('.l-mainHome .inner').parallax({
      imageSrc: '../assets/img/main_bg.png',
      positionY: 'top',
      speed: 1.3
    });
  }
};



$(function() {

  app.init();

});