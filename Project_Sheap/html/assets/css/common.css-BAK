@charset "UTF-8";
/*------------------------------------------------------------
	Default
------------------------------------------------------------*/
html, body, div, span, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
abbr, address, cite, code,
del, dfn, em, img, ins, kbd, q, samp,
small, strong, sub, sup, var,
b, i,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, dialog, figure, footer, header, button,
hgroup, menu, nav, section,
time, mark, audio, video {
  margin: 0;
  padding: 0;
  border: 0;
  outline: 0;
  font-size: 1em;
}

html {
  font-size: 62.5%;
}

body, table,
input, textarea, select, option, button,
h1, h2, h3, h4, h5, h6 {
  font-family: 游ゴシック体, 'Yu Gothic', YuGothic, 'ヒラギノ角ゴシック Pro', 'Hiragino Kaku Gothic Pro', メイリオ, Meiryo, Osaka, 'ＭＳ Ｐゴシック', 'MS PGothic', sans-serif;
  line-height: 1.1;
}

h1, h2, h3, h4, h5, h6 {
  font-weight: bold;
}

table,
input, textarea, select, option {
  line-height: 1.1;
}

ol, ul {
  list-style: none;
}

blockquote, q {
  quotes: none;
}

:focus {
  outline: 0;
}

ins {
  text-decoration: none;
}

del {
  text-decoration: line-through;
}

img {
  vertical-align: top;
}

/*------------------------------------------------------------
	Text Format
------------------------------------------------------------*/
/* text position */
.taLeft {
  text-align: left !important;
}

.taCenter {
  text-align: center !important;
}

.taRight {
  text-align: right !important;
}

.vTop {
  vertical-align: top !important;
}

.vMiddle {
  vertical-align: middle !important;
}

/* font weight */
.fwNormal {
  font-weight: normal !important;
}

.fwBold {
  font-weight: bold !important;
}

/*------------------------------------------------------------
	adjustment class ※ no use frequently
------------------------------------------------------------*/
.mt0 {
  margin-top: 0 !important;
}

.mt10 {
  margin-top: 10px !important;
}

.mb0 {
  margin-bottom: 0 !important;
}

.pl05 {
  padding-left: 5px;
}

.pt75 {
  padding-top: 75px;
}

.pr {
  padding-right: 5px;
}

/*------------------------------------------------------------
	clearfix（float解除）
------------------------------------------------------------*/
.clearfix {
  *zoom: 1;
}

.clearfix:after {
  display: block;
  clear: both;
  content: "";
}

/*------------------------------------------------------------
	for tracking tag
------------------------------------------------------------*/
.trackTags {
  display: none;
}

/*------------------------------------------------------------
	color
------------------------------------------------------------*/
.color-brown {
  color: #b28850;
}

.color-ocean {
  color: #21a5aa;
}

.pull_right {
  float: right;
}

.pull_left {
  float: left;
}

/*------------------------------------------------------------
	Layout
------------------------------------------------------------*/
* {
  -webkit-box-sizing: border-box;
          box-sizing: border-box;
}

html {
  font-size: 62.5%;
}

body {
  /* ウインドウ幅で横スクロール時に背景が切れるバグ対策でコンテンツと同じ幅を指定 */
  line-height: 1.5;
  font-family: "游ゴシック", YuGothic, "ヒラギノ角ゴ Pro", "Hiragino Kaku Gothic Pro", "メイリオ", "Meiryo", sans-serif;
  font-size: 1.4rem;
  font-weight: 500;
  -webkit-text-size-adjust: auto;
  min-width: 1000px;
  color: #202020;
  background: url(../img/body_bg.jpg) no-repeat center top;
}

@media (max-width: 736px) {
  body {
    min-width: auto;
  }
}

.container {
  margin: 0 auto;
  width: 1000px;
  position: relative;
}

@media (max-width: 736px) {
  .container {
    max-width: auto;
    width: 100%;
    padding: 0 10px;
  }
}

.l-main {
  font-size: 1.6rem;
  padding-bottom: 160px;
  background: url(../img/main_bg.jpg) no-repeat center bottom;
}

.l-main:after {
  display: block;
  clear: both;
  content: "";
}

@media (max-width: 736px) {
  .l-main {
    font-size: 1.2rem;
  }
}

@media (max-width: 736px) {
  img {
    max-width: 100%;
  }
}

.clearix:after {
  display: block;
  clear: both;
  content: "";
}

.textCenter {
  text-align: center;
}

/*------------------------------------------------------------
	header
------------------------------------------------------------*/
.l-header {
  height: 709px;
}

.l-headerPage {
  height: auto;
}

.l-header h1 {
  padding-top: 75px;
  padding-left: 285px;
}

.l-header .contactBtn {
  position: absolute;
  right: -116px;
  top: 45px;
  z-index: 99999;
}

.l-sideMenu {
  width: 215px;
  position: absolute;
  left: 12px;
  display: block;
  background: url(../img/menu_sidde_bg.png) no-repeat center top;
  text-align: center;
  padding-top: 50px;
  height: 595px;
  z-index: 99999;
}

.l-sideMenu .nav {
  margin-top: 45px;
}

.l-sideMenu .nav {
  text-align: center;
  display: block;
}

.l-sideMenu .nav li {
  display: inline-block;
  vertical-align: top;
  width: 19px;
  line-height: 1.1;
  word-break: break-all;
}

.l-sideMenu a {
  text-decoration: none;
  color: #333;
}

.l-sideMenu .bottom {
  position: absolute;
  bottom: 35px;
  left: 35px;
  text-align: left;
}

.l-sideMenu .topBtn {
  position: absolute;
  right: -35px;
  top: 0;
}

.headerContent {
  margin-left: 280px;
}

.headerContent #pagePath {
  padding-top: 40px;
}

.headerContent #pagePath ul {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
}

.headerContent #pagePath li {
  color: #000;
}

.headerContent #pagePath li:after {
  content: ">";
}

.headerContent #pagePath li:last-child:after {
  content: "";
}

.headerContent #pagePath a {
  text-decoration: none;
  color: #FCB215;
}

/*------------------------------------------------------------
	Navi
------------------------------------------------------------*/
/*------------------------------------------------------------
	Breadcrumbs
------------------------------------------------------------*/
/*------------------------------------------------------------
	Page title
------------------------------------------------------------*/
/*------------------------------------------------------------
	contentc
------------------------------------------------------------*/
.mainContent {
  margin-left: 280px;
}

/*------------------------------------------------------------
	block
------------------------------------------------------------*/
/*------------------------------------------------------------
	side
------------------------------------------------------------*/
/*------------------------------------------------------------
	footer
------------------------------------------------------------*/
.l-footer {
  background: url(../img/footer_bg.jpg) no-repeat center top;
  padding-top: 80px;
}

.l-footer .footer-top {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
  margin-bottom: 60px;
}

.l-footer .footer-top a {
  color: #fff;
  text-decoration: none;
}

.l-footer .footer-content {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-pack: justify;
      -ms-flex-pack: justify;
          justify-content: space-between;
  padding-bottom: 57px;
}

.l-footer .footer-content h4 {
  margin-bottom: 15px;
}

.l-footer .footer-content ul.fNav {
  margin-left: 10px;
  margin-bottom: 30px;
}

.l-footer .footer-content ul.fNav li + li {
  margin-top: 5px;
}

.l-footer .footer-content ul.socialNav {
  background: url(../img/footer_line.png) no-repeat left bottom;
}

.l-footer .footer-content ul.socialNav li {
  background: url(../img/footer_line.png) no-repeat left top;
  padding: 15px 10px;
}

.l-footer .footer-content a {
  text-decoration: none;
  color: #fff;
}

.l-footer .footer-logo {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
  padding: 15px 0;
}

.l-footer .footer-logo a {
  margin-right: 78px;
}

.l-footer .footer-logo p {
  line-height: 1.7;
}

.l-footer .footer_bottom {
  background: url(../img/footer_bg01.png) no-repeat center top;
  height: 238px;
  color: #fff;
  text-align: right;
}

.l-footer .footer_bottom p {
  padding-top: 200px;
}

/*------------------------------------------------------------
	media
------------------------------------------------------------*/
/*------------------------------------------------------------
	Headline
------------------------------------------------------------*/
.pc-only {
  display: block !important;
}

@media (max-width: 736px) {
  .pc-only {
    display: none !important;
  }
}

.sp-only {
  display: none !important;
}

@media (max-width: 736px) {
  .sp-only {
    display: block !important;
  }
}

.pc-break {
  display: block;
}

@media (max-width: 736px) {
  .pc-break {
    display: inline;
  }
}

.sp-break {
  display: inline;
}

@media (max-width: 736px) {
  .sp-break {
    display: block;
  }
}

.clearfix:after {
  display: block;
  clear: both;
  content: "";
}

.mt30 {
  margin-top: 30px;
}

.mt35 {
  margin-top: 35px;
}

.mt100 {
  margin-top: 100px;
}

.mr35 {
  margin-right: 35px;
}

.mr45 {
  margin-right: 45px;
}

.mb55 {
  margin-bottom: 55px;
}

.mb60 {
  margin-bottom: 60px;
}

.mb70 {
  margin-bottom: 70px;
}

.mb05 {
  margin-bottom: 5px;
}

.mb15 {
  margin-bottom: 15px;
}

.mb20 {
  margin-bottom: 20px;
}

.mb110 {
  margin-bottom: 110px;
}

.mb120 {
  margin-bottom: 120px;
}

.mb25 {
  margin-bottom: 25px;
}

.pt50 {
  padding-top: 50px;
}

.ml35 {
  margin-left: 35px;
}

.ml60 {
  margin-left: 60px;
}

.pt40 {
  padding-top: 40px;
}

.pt55 {
  padding-top: 55px;
}

.pt35 {
  padding-top: 35px;
}

/*------------------------------------------------------------
	title
------------------------------------------------------------*/
/*------------------------------------------------------------
	component
------------------------------------------------------------*/
.l-main {
  margin-top: -30px;
  padding-top: 75px;
}

.l-mainHome {
  background: url(../img/main_bg.png) no-repeat center top;
}

.c-pageTitle {
  margin-bottom: 30px;
}

/*------------------------------------------------------------
	Slider
------------------------------------------------------------*/
