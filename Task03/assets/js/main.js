$( document ).ready(function() {

  if (navigator.userAgent.match(/msie/i) || navigator.userAgent.match(/trident/i) ){      $("html").addClass("ie");  }

  $('.btn-menu').on('click', function() {
    $('#gNavSP').slideToggle();
  });
  $('.btn-close').on('click', function() {
    $('#gNavSP').slideUp();
  });
  $('.has-sub').on('click', function() {
    $(this).toggleClass('is-active');
    $(this).next().slideToggle();
  });
});