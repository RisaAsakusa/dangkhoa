$( document ).ready(function() {

  $('.btn-menu').on("click", function() {
    $(this).toggleClass('is-active');
    $('#gNavSP').slideToggle();
  });

  $('.close').on("click", function() {
    $('.btn-menu').removeClass('is-active');
    $('#gNavSP').slideUp();
  });

  $(window).scroll(function(){
    var h = $(window).height();
    if($(this).scrollTop() > (h / 2)) {
      $('.navFixSp').addClass('is-active');
      $('.navFix').addClass('is-active');
    } else {
      $('.navFixSp').removeClass('is-active');
      $('.navFix').removeClass('is-active');
    }


  });

  $(".slider").slick({
    dots: true,
    infinite: true,
    autoplay: true,
    speed: 1000,
    autoplaySpeed: 6000,
    slidesToShow: 1,
    centerMode: true,
    prevArrow: false,
    nextArrow: false,
    pauseOnHover: false,
    variableWidth: true
  });

  $(".slider_sp").slick({
    dots: true,
    infinite: true,
    autoplay: true,
    speed: 1000,
    autoplaySpeed: 6000,
    slidesToShow: 1,
    centerMode: true,
    prevArrow: false,
    nextArrow: false,
    fade: true,
    cssEase: 'linear'
  });

  $('.c-recruitList').slick({
    infinite: false,
    dots: false,
    arrows: false,
    speed: 500,
    slidesToShow: 3,
    variableWidth: true,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          arrows: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: true,
          infinite: true, 
          variableWidth: true,
        }
      }
    ]
  });

  var tabMenu = $('.tabs');
  var tabContent = $('.tabs-content');

  tabContent.find('#tab01').addClass('is-active');

  tabMenu.find('li').on("click", function(e) {
    e.preventDefault();
    var tab = $(this).attr('data-tab');
    tabMenu.find('li').removeClass('is-active');
    $(this).addClass('is-active');
    tabContent.find('.tabs-item').removeClass('is-active');
    tabContent.find('.tabs-item' + tab).addClass('is-active');
  });

  var moreBtn = $('.tabs-content .more');
  moreBtn.on("click", function(e) {
    e.preventDefault();
    var $this = $(this),
        $chilA = $(this).find('.button');
    $this.toggleClass('is-active');
    var text = $chilA.text();
    $chilA.text(
      text == "さらに条件を追加する" ? "詳細条件を閉じる" : "さらに条件を追加する");
    $this.find('.moreContent').slideToggle('fast');
  });

  $('[data-popup-open]').on('click', function(e) {
		var targeted_popup_class = $(this).attr('data-popup-open');
		$('[data-popup="' + targeted_popup_class + '"]').fadeIn(350);

		e.preventDefault();
	});

	//----- CLOSE
	$('[data-popup-close]').on('click', function(e) {
		var targeted_popup_class = $(this).attr('data-popup-close');
		$('[data-popup="' + targeted_popup_class + '"]').fadeOut(350);
		e.preventDefault();
  });
  


});
