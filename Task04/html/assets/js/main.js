$( document ).ready(function() {

  if (navigator.userAgent.match(/msie/i) || navigator.userAgent.match(/trident/i) ){      $("html").addClass("ie");  }

  
  $('.faqItems .q').on('click', function(e) {
    $(this).toggleClass('is-active');
    $(this).next().slideToggle();
  });

  $('.btn-menu').on('click', function(e) {
    $(this).toggleClass('is-active');
    $(this).next().slideToggle();
  });

  var btnTop = $('#btn-top');

  btnTop.click(function() {
    $('html, body').animate({
      scrollTop: 0
    }, 500);
  });

  btnTopFade();
  btnTopFixed();

  $(window).on('load scroll resize', function() {
    btnTopFade();
    btnTopFixed();
  });

  function btnTopFade() {
    if ($(window).scrollTop() > $(window).height() * 0.2) {
      if (!btnTop.is(':visible')) {
        btnTop.css('opacity', 0).show();
        btnTop.animate({
          opacity: 1
        }, 400);
      }
    } else {
      if (btnTop.is(':visible') && !btnTop.is(':animated')) {
        btnTop.animate({
          opacity: 0
        }, 400, function() {
          btnTop.css('opacity', 1).hide();
        });
      }
    }
  }

  function btnTopFixed() {
      var gutter = 20;
      var footer = $('footer');
      var footerLine = $('html').height() - footer.outerHeight() - gutter;
      var winBottomLine = $(window).scrollTop() + $(window).height();
      var distance = winBottomLine - footerLine;
      if (distance > gutter) {
        btnTop.css('bottom', distance + 'px');
      } else {
        btnTop.css('bottom', gutter + 'px');
      }
    
  }
});